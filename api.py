import asyncio
from aiohttp.web import Application, Response
from functools import wraps
from pymongo import *

class webapp(Application):
    """
    Extending Application to add decorator sugar
    """

    def route(self, rule, **options):
        def decorator(f):
            endpoint = options.pop('endpoint', None)
            self.router.add_route('GET', rule, f, **options)
            return f
        return decorator

    def templated(template=None):
        def decorator(f):
            @wraps(f)
            def decorated_function(*args, **kwargs):
                template_name = template
                if template_name is None:
                    template_name = request.endpoint \
                        .replace('.', '/') + '.html'
                ctx = f(*args, **kwargs)
                if ctx is None:
                    ctx = {}
                elif not isinstance(ctx, dict):
                    return ctx
                return render_template(template_name, **ctx)
            return decorated_function
        return decorator

app = webapp()
client = MongoClient()
db = client.test_database

response = {'housenum':'52',
            'city':    'York',
            'postcode':'YO31 0UH'}


@asyncio.coroutine
def hello(request):
    return Response(body=b"Hello, world")

@asyncio.coroutine
def variable_handler(request):
    return Response(
        text="Hello, {}".format(request.match_info['name']))

@app.route("/hi")
@asyncio.coroutine
def hi(request):
    return Response(text=(str(response)))

@app.route("/to/{to}/from/{from}")
@asyncio.coroutine
def greet(request):
    return Response(body="To {to}, from {from}".format(**request.match_info))

app.router.add_route('GET', '/', hello)
app.router.add_route('GET', '/hello/{name}', variable_handler)



@app.route("/house/{to}/from/{from}")
def get_houses(request):
    pass

@app.route("/city/{city}")
def get_city(request):
    print (request.match_info['city'])
    return Response(text="ok")


loop = asyncio.get_event_loop()
f = loop.create_server(app.make_handler(), '0.0.0.0', 8080)
srv = loop.run_until_complete(f)
# print(app.router._urls)
print('serving on', srv.sockets[0].getsockname())
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

house
    number/name(str)
    street(str)
    postcode(str)
    average_rating(int)
    town(link)
    neighbourhood(link)
    review(list link)
    picture(list str)
    landlord(link)

town
    
review
    house(link)
    rating(str)
    title(str)
    text(str)
    